package com.sandbox.pageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Page Object of Home page
 */
public class HomePage {

    @FindBy(tagName = "ml-item-card")
    public List<WebElement> items;

    @FindBy(xpath = ".//ml-item-card//div[@class='brand']")
    public List<WebElement> itemsBrand;

    @FindBy(xpath = ".//input[contains(@class, 'search-by-text')]")
    public WebElement searchField;

    @FindBy(className = "no-results")
    public WebElement searchError;

    @FindBy(xpath = ".//ml-filter-group[@ng-reflect-title='color_concept']")
    public WebElement colorBlock;

    @FindBy(xpath = ".//label[@title='Black']//span[@class='checkbox-mark']")
    public WebElement blackFilter;

    @FindBy(xpath = ".//ml-filter-group[@ng-reflect-title='brand_concept']")
    public WebElement brandBlock;

    @FindBy(xpath = ".//label[@title='Calvin Klein']//span[@class='checkbox-mark']")
    public WebElement calvinKleinFilter;

    @FindBy(xpath = ".//div[@class='active-filter']/span")
    public WebElement activeFilterLabelName;

    @FindBy(xpath = ".//label[@title='1.STATE']//span[@class='checkbox-mark']")
    public WebElement OneStateFilter;

    public void search(String text) {
        searchField.sendKeys(text);
        searchField.sendKeys(Keys.ENTER);
    }

    public void selectBlackFilter() {
        blackFilter.click();
    }

    public void selectCalvinKleinFilter() {
        calvinKleinFilter.click();
    }

    public void select1STATEBrand(){ OneStateFilter.click();}

}