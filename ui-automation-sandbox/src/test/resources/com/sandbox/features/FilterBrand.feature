Feature: FilterBrand
  As a user
  I can choose brand

  @p2
  Scenario: value's brand are displayed
    Given user opens Home page
    When user choose 1Brand
    Then 1State brand filter is selected
    And 20 items are displayed on the page
    And all displayed items belong to brand 1Brand
