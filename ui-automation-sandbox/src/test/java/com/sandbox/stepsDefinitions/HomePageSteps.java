package com.sandbox.stepsDefinitions;


import com.sandbox.Hooks;
import com.sandbox.pageObjects.HomePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Step Definitions for Home page
 */
public class HomePageSteps {

    private static final String BASE_URL = "http://localhost:80";

    protected WebDriver driver = Hooks.driver;
    WebDriverWait wait = new WebDriverWait(driver, 10);

    @Given("user opens Home page")
    public void openHomePage() throws Throwable {
        driver.get(BASE_URL);
    }

    @When("user searches '([^']+)'")
    public void search(String text) throws Throwable {
        homePage().search(text);
    }

    @Then("search results are displayed")
    public void verifySearchResultsDisplayed() throws Throwable {
        assertThat(homePage().items.size()).isNotZero();
    }

    @Then("(\\d*) items are displayed on the page")
    public void verifyCorrectItemsQtyDisplayed(int qty) throws Throwable {
        assertThat(homePage().items.size()).isEqualTo(qty);
    }

    @Then("error message '([^']+)' is displayed")
    public void verifyErrorMessage(String text) throws Throwable {
        wait.until(ExpectedConditions.visibilityOf(homePage().searchError));
        assertThat(homePage().searchError.getText().trim()).contains(text);
    }

    @When("user applies Black color filter")
    public void applyColorFilter() throws Throwable {
        wait.until(ExpectedConditions.elementToBeClickable(homePage().blackFilter));
        homePage().selectBlackFilter();
    }

    @Then("([^']+) color filter is selected")
    public void verifyColorFilterIsSelected(String filterName) throws Throwable {
        assertThat(homePage().colorBlock.getAttribute("ng-reflect-external-state").equals(filterName));
    }

    @When("user applies Calvin Klein brand filter")
    public void applyCalvinKleinFilter() throws Throwable {
        wait.until(ExpectedConditions.elementToBeClickable(homePage().calvinKleinFilter));
        homePage().selectCalvinKleinFilter();
    }

    @Then("([^']+) brand filter is selected")
    public void verifyBrandFilterIsSelected(String filterName) throws Throwable {
        assertThat(homePage().brandBlock.getAttribute("ng-reflect-external-state").equals(filterName.toLowerCase()));
    }

    @Then("active filter label ([^']+) is displayed")
    public void verifyActiveFilterIsDisplayed(String filterName) throws Throwable {
        assertThat(homePage().activeFilterLabelName.getText().equals(filterName.toLowerCase()));
    }

    @Then("all displayed items belong to brand ([^']+)")
    public void verifyItemsWithBrandAreDisplayed(final String brand) throws Throwable {
        homePage().itemsBrand.forEach(item -> {
            assertThat(item.getAttribute("title").matches(brand));
        });
    }

    @When("user choose 1Brand")
        public void apply1BrandFilter() throws Throwable {
        wait.until(ExpectedConditions.elementToBeClickable(homePage().OneStateFilter));
            homePage().select1STATEBrand();
        }



    private HomePage homePage() {
        return PageFactory.initElements(driver, HomePage.class);
    }

}